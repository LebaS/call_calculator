from django.contrib import admin
from django.urls import path
from called import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='docs'),
    path('api/billings/', views.BillingList, name='billing_api'),
    path('api/call/',views.CallRecordInput, name= 'calls_api'),
]
urlpatterns += staticfiles_urlpatterns()
