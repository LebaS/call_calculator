# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
from django.db import models
from django.core.validators import RegexValidator
from datetime import datetime
from called.validators import validate_call_id, validate_call_type, validate_call_timestamp, validate_phone_number

# model for recording start and end of telephone calls
class CallRecord(models.Model):
  CALL_LIST = (('S','START'), ('E','END'))

  id = models.AutoField(primary_key = True, verbose_name = 'ID')
  call_id = models.CharField(null = False, blank = False, max_length = 255, validators = [validate_call_id], verbose_name = 'Call ID')
  call_type = models.CharField(choices = CALL_LIST, null = False, blank= False, max_length= 6, validators = [validate_call_type], verbose_name = 'Call Type')
  timestamp = models.DateTimeField(null = False, blank = False, validators = [validate_call_timestamp], default = datetime.now(), verbose_name = 'Timestamp')
  source = models.CharField(validators = [validate_phone_number], max_length = 11, verbose_name = 'Call Source')
  destination = models.CharField(validators = [validate_phone_number], max_length = 11, verbose_name = 'Call Destination')
  created_at = models.DateTimeField(auto_now_add = True, null = False, editable= False, verbose_name = 'Created_at')

  class Meta:
    verbose_name = '[CALL] Record'
    verbose_name_plural = verbose_name
    ordering = ['created_at']
    unique_together = ['call_type', 'call_id']

  def __str__(self):
    return '[ %s ] %s'%(self.call_id, self.call_type.upper())

# model collection policy by range of schedules
class CallPricingRule(models.Model):
  name = models.CharField(unique = True, null = False, blank = False, max_length = 100, default = 'normal', verbose_name = 'Name')
  price_standing = models.IntegerField(null = False, blank = False, default = 0, verbose_name = 'Standing Charge')
  price_minute = models.IntegerField(null = False, blank = False, default = 0, verbose_name = 'Call charge / minute')
  time_start = models.TimeField(null = False, blank = False, verbose_name = 'Time start')
  time_end = models.TimeField(null = False, blank = False, verbose_name = 'Time end')
  created_at = models.DateTimeField(auto_now_add = True, null = False, editable = False, verbose_name = 'Created_at')

  class Meta:
    verbose_name = '[CALL] Pricing Rules'
    verbose_name_plural = verbose_name
    ordering = ['name']

  def __str__(self):
    return '%s [%s -> %s]'%(self.name, self.time_start, self.time_end)
