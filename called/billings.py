# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
from .models import CallRecord, CallPricingRule
from django.db.models import Q
from datetime import datetime
import calendar


# class for usage each charge call from the collection
class Call:
  # parameter : call_id
  def __init__(self, call_id):
    self.calls = CallRecord.objects.filter(call_id=call_id) if call_id is not None else None
    self.call_begin, self.call_end = self.calls.get(call_type='START'), self.calls.get(call_type='END')
    if self.call_begin:
      self.destination = self.call_begin.destination
      self.source = self.call_begin.source
      self.date_start = self.call_begin.timestamp.date().strftime("%Y-%m-%d")
      self.time_start = self.call_begin.timestamp.time().strftime("%H:%M:%S")
      self.id = self.call_begin.call_id

  # Call cost calculation
  # get Price Rule for call
  def cost(self):
    for period in CallPricingRule.objects.all():
      if period.time_start < period.time_end:
        if (period.time_start <= self.call_begin.timestamp.time()) and (period.time_end > self.call_begin.timestamp.time()):
          self.call_price = period
      else:
        if (period.time_start <= self.call_begin.timestamp.time()) or (period.time_end > self.call_begin.timestamp.time()):
          self.call_price = period
    return (self.__duration__() * self.call_price.price_minute * 0.01) + (self.call_price.price_standing * 0.01)

  # determine the period of the call
  def period(self):
    if self.calls is not None:
      y, m = self.call_end.timestamp.strftime("%Y-%m").split('-')
      return datetime(int(y), int(m), 1)

  # determine the duration of the call ( in integer )
  def __duration__(self):
    return int((self.call_end.timestamp - self.call_begin.timestamp).total_seconds() / 60.0)

  # determine the duration of the call ( format in 0h 0m 0s )
  def duration_format(self):
    days = divmod((self.call_end.timestamp - self.call_begin.timestamp).total_seconds(), 86400)
    hours = divmod(days[1], 3600)
    minutes = divmod(hours[1], 60)
    seconds = divmod(minutes[1], 1)
    return "%iday(s) %ih %im %is"%(days[0],hours[0], minutes[0], seconds[0])


# Class to control the collection in the determined period
class Billing(object):
  # parameter phone_number = source phone number
  def __init__(self, phone_number, period=None):
    self.total = 0.0
    self.call = []
    self.number = 0
    self.phone = phone_number
    period = self.__period_info__(period)
    for item in CallRecord.objects.filter(source=self.phone).values('call_id').distinct():
      call = Call(item.get('call_id'))
      #print("[%s] %s -> %s"%(item['call_id'], call.period(), period))
      if call.period() == period:
        self.call.append(call)
        self.total += call.cost()
        self.number += 1

  # determine the period in billing
  # if empty period, the last closed period will be displayed
  def __period_info__(self, period):
    if period:
      y, m = period.split('-')
    else:
      m, y = datetime.now().month, datetime.now().year
      last_start = CallRecord.objects.filter(source=self.phone, call_type='START', timestamp__lt=datetime(int(y),int(m), 1)).order_by('timestamp').last()
      if last_start is not None:
        last_end = CallRecord.objects.get(call_type='END', call_id=last_start.call_id)
        m, y = (last_end.timestamp.month, last_end.timestamp.year)
      else:
        m, y = (datetime.now().month, datetime.now().year)
    return datetime(int(y), int(m), 1)

  # format for JSON billing summary
  def to_json(self):
    data = {"billing":{"total_items": self.number, "total_price": '$ {:5,.2f}'.format(self.total), "phone_number": self.phone, "list_call":[]}}
    for item in self.call:
      data['billing']['list_call'].append({"destination": item.destination, "date_start": item.date_start, "time_start": item.time_start, "id": item.id, "duration": item.duration_format(), "price": '$ {:5,.2f}'.format(item.cost())})
    return data
