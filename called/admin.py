from django.contrib import admin
from .models import CallRecord, CallPricingRule

class CallRecordAdmin(admin.ModelAdmin):
  list_per_page = 50
  #list_filter = []
  #list_display = []
  #ordering = []
  #search_fields = []

class CallPricingRuleAdmin(admin.ModelAdmin):
  ordering = ['time_start']

admin.site.register(CallRecord, CallRecordAdmin)
admin.site.register(CallPricingRule, CallPricingRuleAdmin)
