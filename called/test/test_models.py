# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-

from datetime import datetime
from django.test import TestCase
from called.models import CallRecord, CallPricingRule
from django.utils import timezone
import time


class CallRecordTest(TestCase):
	# params = {'call_id' : '', 'call_type' : '', 'timestamp' : '', 'source' : '', 'destination' : ''}
	def create_call_record(self, **params):
		if params:
			if params['call_type'] is 'START':
				return CallRecord.objects.create(call_id = params['call_id'], call_type = params['call_type'], timestamp = params['timestamp'], source = params['source'], destination = params['destination'])
			elif params['call_type'] is 'END':
				return CallRecord.objects.create(call_id = params['call_id'], call_type = params['call_type'], timestamp = params['timestamp'])

	# params = {'name' : '', 'price_standing' : '', 'price_minute' : '', 'time_start' : '', 'time_end' : ''}
	def create_call_pricing_rule(self, **params):
		return CallPricingRule.objects.create(name=params['name'], price_standing=params['price_standing'], price_minute=params['price_minute'], time_start=params['time_start'], time_end=params['time_end']) if params else None

	# create records for call
	def test_call_record(self):
		w1 = self.create_call_record(call_id = '#328', call_type = 'START', timestamp = datetime(2018, 8, 11, 15, 21), source = '21998366257', destination = '21987425379')
		self.assertTrue(isinstance(w1, CallRecord))
		self.assertEqual(w1.__str__(), '[ %s ] %s'%(w1.call_id, w1.call_type))

		w2 = self.create_call_record(call_id = '#328', call_type = 'END', timestamp = datetime(2018, 8, 11, 15, 29))
		self.assertTrue(isinstance(w2, CallRecord))

	# create records for pricing rules
	def test_pricing_rules(self):
		CallPricingRule.objects.all().delete()
		p1 = self.create_call_pricing_rule(name = 'standard time call', price_standing=36, price_minute = 9, time_start = '06:00', time_end = '22:00' )
		p2 = self.create_call_pricing_rule(name = 'reduced tariff time call', price_standing=36, price_minute = 0, time_start = '22:00', time_end = '06:00' )
		self.assertTrue(isinstance(p1,CallPricingRule))
		self.assertTrue(isinstance(p2,CallPricingRule))

		self.assertEqual(p1.__str__(), '%s [%s -> %s]'%(p1.name,p1.time_start,p1.time_end))
		self.assertEqual(p2.__str__(), '%s [%s -> %s]'%(p2.name,p2.time_start,p2.time_end))

	# create records for call with real datas
	def real_test(self):
		c1 = self.create_call_record(call_id = '70', call_type='start', timestamp= datetime(2016,2,29,12,0,0),  source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id=  '70', call_type='end', timestamp= datetime(2016,2,29,14,0,0))
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '71', call_type='start', timestamp= datetime(2017,12,12,15,7,13), source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '71', call_type='end', timestamp= datetime(2017,12,12,15,14,56))
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '72', call_type='start', timestamp= datetime(2017,12,12,22,47,56), source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '72', call_type='end', timestamp= datetime(2017,12,12,22,50,56))
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '73', call_type='start', timestamp= datetime(2017,12,12,21,57,13), source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '73', call_type='end', timestamp= datetime(2017,12,12,22,10,56))
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '74', call_type='start', timestamp= datetime(2017,12,12,4,57,13), source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '74', call_type='end', timestamp= datetime(2017,12,12,6,10,56))
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '75', call_type='start', timestamp= datetime(2017,12,12,21,57,13), source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '75', call_type='end', timestamp= datetime(2017,12,13,22,10,56))
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '76', call_type='start', timestamp= datetime(2017,12,12,15,7,58), source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '76', call_type='end', timestamp= datetime(2017,12,12,15,12,56))
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '77', call_type='start', timestamp= datetime(2018,2,28,21,57,13), source='99988526423', destination='9993468278')
		self.assertTrue(isinstance(c1, CallRecord))
		c1 = self.create_call_record(call_id = '77', call_type='end', timestamp= datetime(2018,3,1,22,10,56))
		self.assertTrue(isinstance(c1, CallRecord))
