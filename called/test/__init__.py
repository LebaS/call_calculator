# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
import unittest
from pyunitreport import HTMLTestRunner
from .test_models import CallRecordTest
from .test_billings import CallBillingTest
from .test_requests import RequestTest

def call_suite():
  suite, result = unittest.TestSuite(), unittest.TestResult()
  suite.addTest(unittest.makeSuite(CallRecordTest))
  suite.addTest(unittest.makeSuite(CallBillingTest))
  suite.addTest(unittest.makeSuite(RequestTest))
  kwargs = {"output": "suite/", "report_name": "suite_report", "failfast": True}
  runner = HTMLTestRunner(**kwargs)
  runner.run(suite)

call_suite()
