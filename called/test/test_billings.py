# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
from datetime import datetime
from django.test import TestCase
from called.models import CallRecord, CallPricingRule
from called.billings import Billing, Call
from django.utils import timezone
import time
import json
import requests


class CallBillingTest(TestCase):

  def test_call_class(self):
    self.call_id = '169'
    params = {'id':self.call_id,'source':'998366257','destination':'969521376','timestamp':'2018-08-08 12:51:01','type':'START'}
    resp = requests.post("http://localhost:8000/api/call/", json=params)
    #self.assertEqual(resp.status_code, 201)

    params = {'id':self.call_id,'timestamp':'2018-08-08 13:00:09','type':'END'}
    resp = requests.post("http://localhost:8000/api/call/", json=params)
    #self.assertEqual(resp.status_code, 201)

    self.call = Call(self.call_id)

    self.assertEqual(self.call.period(),datetime(2018, 8, 1, 0, 0))
    self.assertEqual(self.call.duration_format(),'0day(s) 0h 9m 8s')
    self.assertEqual(self.call.cost(),1.17)

  def test_billing_total_cost(self):
    self.billing = Billing(phone_number='998366257',period='2018-08').to_json()
    self.assertEqual(self.billing.get('billing').get('total_items'),2)
    self.assertEqual(self.billing.get('billing').get('total_price'), '$  2.88')

    self.assertEqual(self.billing.get('billing').get('list_call'), [
      {'destination': '969521376', 'date_start': '2018-08-10', 'time_start': '16:50:01', 'id': '130', 'duration': '0day(s) 0h 15m 0s', 'price': '$  1.71'},
      {'destination': '969521376', 'date_start': '2018-08-08', 'time_start': '12:51:01', 'id': '169', 'duration': '0day(s) 0h 9m 8s', 'price': '$  1.17'}
    ])

  def test_scenario_with_period(self):
    b1 = Billing(phone_number=99988526423, period='2017-12')
    result_json = b1.to_json()
    self.assertTrue(isinstance(b1, Billing))
    self.assertEqual(result_json.get('billing').get('total_price'), '$ 135.09')
    self.assertEqual(result_json.get('billing').get('total_items'), 6)
    self.assertEqual(len(result_json.get('billing').get('list_call')), result_json.get('billing').get('total_items'))

  def test_scenario_without_period(self):
    b1 = Billing(phone_number=99988526423)
    result_json = b1.to_json()
    self.assertTrue(isinstance(b1, Billing))
    result_json = b1.to_json()
    self.assertEqual(result_json.get('billing').get('total_price'), '$ 131.13')
    self.assertEqual(result_json.get('billing').get('total_items'), 1)
    self.assertEqual(len(result_json.get('billing').get('list_call')), result_json.get('billing').get('total_items'))

