# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
from django.test import TestCase
import json
import requests


class RequestTest(TestCase):
  def test_call_record_get_api(self):
    resp = requests.get("http://localhost:8000/api/call/")
    self.assertEqual(resp.text, '{"detail":"Method \\"GET\\" not allowed."}')

  def test_index_get(self):
    resp = requests.get("http://localhost:8000")
    self.assertEqual(resp.status_code, 200)

  def test_billing_get(self):
    resp = requests.get("http://localhost:8000/api/billings/")
    self.assertEqual(resp.text, '{"detail":"Method \\"GET\\" not allowed."}')

  def test_get_billing_with_period(self):
    params = {'source':'998366257','period':'2018-08'}
    resp = requests.post("http://localhost:8000/api/billings/", json=params)
    #self.assertEqual(resp.status_code, 201)

  def test_get_biling_witout_period(self):
    params = {'source':'998366257'}
    resp = requests.post("http://localhost:8000/api/billings/", json=params)
    #self.assertEqual(resp.status_code, 201)

  # with request
  def test_request_billing_with_period(self):
    params = {'source':'998366257','period':'2018-08'}
    resp = requests.post("http://localhost:8000/api/billings/", json=params)
    #self.assertEqual(resp.status_code, 201)
    resp_json = json.loads(resp.text)
    self.assertEqual(resp_json.get('result').get('billing').get('total_items'),2)
    self.assertEqual(resp_json.get('result').get('billing').get('total_price'),"$  2.88")

  def test_request_biling_witout_period(self):
    params = {'source':'998366257'}
    resp = requests.post("http://localhost:8000/api/billings/", json=params)
    #self.assertEqual(resp.status_code, 201)
    resp_json = json.loads(resp.text)
    self.assertEqual(resp_json.get('result').get('billing').get('total_items'),2)
    self.assertEqual(resp_json.get('result').get('billing').get('total_price'),"$  2.88")

