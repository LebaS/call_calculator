# Generated by Django 2.0.6 on 2018-09-03 16:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('called', '0003_auto_20180903_1017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='callrecord',
            name='call_type',
            field=models.CharField(choices=[('S', 'START'), ('E', 'END')], max_length=6, verbose_name='Call Type'),
        ),
        migrations.AlterField(
            model_name='callrecord',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 3, 13, 32, 9, 57550), verbose_name='Timestamp'),
        ),
    ]
