# -*- coding: utf-8 -*-
from django.db import migrations, models
from datetime import datetime

def callrecord_info(apps, schema_editor):
  CallRecord = apps.get_model('called', 'CallRecord')
  CallRecord.objects.create(call_id = '70', call_type='start', timestamp= datetime(2016,2,29,12,0,0),  source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id=  '70', call_type='end', timestamp= datetime(2016,2,29,14,0,0))
  CallRecord.objects.create(call_id = '71', call_type='start', timestamp= datetime(2017,12,12,15,7,13), source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id = '71', call_type='end', timestamp= datetime(2017,12,12,15,14,56))
  CallRecord.objects.create(call_id = '72', call_type='start', timestamp= datetime(2017,12,12,22,47,56), source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id = '72', call_type='end', timestamp= datetime(2017,12,12,22,50,56))
  CallRecord.objects.create(call_id = '73', call_type='start', timestamp= datetime(2017,12,12,21,57,13), source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id = '73', call_type='end', timestamp= datetime(2017,12,12,22,10,56))
  CallRecord.objects.create(call_id = '74', call_type='start', timestamp= datetime(2017,12,12,4,57,13), source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id = '74', call_type='end', timestamp= datetime(2017,12,12,6,10,56))
  CallRecord.objects.create(call_id = '75', call_type='start', timestamp= datetime(2017,12,12,21,57,13), source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id = '75', call_type='end', timestamp= datetime(2017,12,13,22,10,56))
  CallRecord.objects.create(call_id = '76', call_type='start', timestamp= datetime(2017,12,12,15,7,58), source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id = '76', call_type='end', timestamp= datetime(2017,12,12,15,12,56))
  CallRecord.objects.create(call_id = '77', call_type='start', timestamp= datetime(2018,2,28,21,57,13), source='99988526423', destination='9993468278')
  CallRecord.objects.create(call_id = '77', call_type='end', timestamp= datetime(2018,3,1,22,10,56))

def callpricing_info(apps, schema_editor):
  CallPricingRule = apps.get_model('called', 'CallPricingRule')
  CallPricingRule.objects.create(name = 'standard time call', price_standing=36, price_minute = 9, time_start = '06:00', time_end = '22:00' )
  CallPricingRule.objects.create(name = 'reduced tariff time call', price_standing=36, price_minute = 0, time_start = '22:00', time_end = '06:00' )

class Migration(migrations.Migration):
    initial = True
    dependencies = [
        ('called', '0006_auto_20180905_1808'),
    ]

    operations = [
        migrations.RunPython(callrecord_info),
        migrations.RunPython(callpricing_info),
    ]
