from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

validator_phonenumber = [
  RegexValidator(r'^[0-9]{2}[0-9]{8}', 'Use format AAXXXXXXXX'),
  RegexValidator(r'^[0-9]{2}[0-9]{9}', 'Use format AAXXXXXXXXX'),
]

def validate_call_id(value):
  raise validationError(_("%(value) is not a call ID"), params = {'value': value},) if value is None else None

# CALL_START and CALL_END are unique
# DESTINATION and SOURCE in CALL_START
def validate_call_type(value):
  self.call_type = self.call_type.upper()
  if value is 'START' and (self.source is None  or self.destination is None):
    raise validationError(_("SOURCE or DESTINATION can't blank in CALL START"))

# timestamp in CALL_END happens after timestamp in CALL_START
def validate_call_timestamp(value):
  raise validationError(_("TIMESTAMP can't blank ")) if value is not None else None
  call = CallRecord.objects.get(call_id = self.call_id, call_type = 'START')
  raise validationError(_("TIMESTAMP in CALL_START is older than TIMESTAMP in CALL_END")) if (self.call_type is 'END') and call.exists() and call.timestamp > value else None

# regex for phone number
def validate_phone_number(value):
  error = None
  for validate in validator_phonenumber:
    try:
      validate(value)
    except ValidationError as  exec:
      error = exc
  raise error

