# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from django.http import JsonResponse
from django.shortcuts import render
from django.conf import settings
from datetime import datetime

from called.models import CallRecord
from called.billings import Call, Billing

def index(request):
  return render(request, 'doc/index.html')

@api_view(['POST'])
def BillingList(request):
  if request.method == 'POST' and 'source' in request.data.keys():
    period = request.data['period'] if 'period' in request.data.keys() else None
    billing = Billing(phone_number = request.data['source'], period = period)
    return JsonResponse({ 'result': billing.to_json() })
  else:
    return Response(status = 400)

@api_view(['POST'])
def CallRecordInput(request):
  if request.method == 'POST':
    f_mask = "%Y-%m-%d %H:%M:%S"
    call = CallRecord(call_id = request.data['id'], call_type=request.data['type'].upper(), timestamp=datetime.strptime(request.data['timestamp'], f_mask))
    if call.call_type == 'START':
      call.source = str(request.data['source'])
      call.destination = str(request.data['destination'])
    call.save()
    return JsonResponse({'status' : 'ok'}, status = 201)
  return Response(status = 400)
